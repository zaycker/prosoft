import fieldWrapper from 'enhancers/fieldWrapper';
import { Input } from 'reactstrap';

export default fieldWrapper(Input);
