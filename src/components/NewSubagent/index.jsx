import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form as BootstrapForm,
  FormGroup,
  Label,
} from 'reactstrap';
import { Form, Field } from 'react-final-form';
import { required } from 'utils/validators';
import InputField from 'components/InputField';
import { compose, withProps } from 'recompose';
import reduxSubmitHandler from 'enhancers/reduxSubmitHandler';
import { connect } from 'dva';
import {
  addSubagent,
} from 'models/subagents/actions';

const NewSubagentModal = ({
  dirtySinceLastSubmit,
  handleSubmit,
  pristine,
  invalid,
  submitting,
}) => (
  <Modal isOpen>
    <ModalHeader>
      Добавление субагента
    </ModalHeader>
    <ModalBody>
      <BootstrapForm onSubmit={handleSubmit}>
        <FormGroup>
          <Label>
            Имя
            <Field
              component={InputField}
              name="name"
              validate={required}
            />
          </Label>
        </FormGroup>
      </BootstrapForm>
    </ModalBody>
    <ModalFooter>
      <Button
        color="primary"
        disabled={pristine || (invalid && !dirtySinceLastSubmit) || submitting}
      >
        Добавить
      </Button>
    </ModalFooter>
  </Modal>
);

NewSubagentModal.propTypes = {
  dirtySinceLastSubmit: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default compose(
  connect(({ subagents, loading }) => ({
    isSubmitComplete: !loading.models.subagents,
    submitError: subagents.error || null,
  }), {
    submitAction: addSubagent,
  }),
  withProps({
    component: NewSubagentModal,
  }),
  reduxSubmitHandler,
)(Form);
