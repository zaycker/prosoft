import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import { setPropTypes } from 'recompose';

export default setPropTypes({
  href: PropTypes.string.isRequired,
})(({ href, ...props }) => (
  <Link to={href} {...props} />
));
