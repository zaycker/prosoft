export const get = (key) => {
  try {
    return JSON.parse(localStorage.getItem(key));
  } catch (e) {
    return null;
  }
};

export const reset = key => localStorage.removeItem(key);
export const set = (key, value) => localStorage.setItem(key, JSON.stringify(value));

export default {
  get,
  reset,
  set,
};
