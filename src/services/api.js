import isomorphicFetch from 'dva/fetch';
import fetchMock from 'fetch-mock';
import { effects } from 'dva/saga';
import { getToken } from 'models/auth';
import 'mocks';

// eslint-disable-next-line prefer-destructuring
const env = process.env;

const fetch = env.REACT_APP_MOCKS === '1' ? fetchMock.fetchHandler : isomorphicFetch;

function formatUrl(url = '') {
  if (/^(?:\w{2,}:)?\/\//i.test(url)) {
    return url;
  }

  return `//${env.REACT_APP_API_HOST}${env.REACT_APP_API_ROOT}${url}`;
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

function parseJSON(response) {
  return response.json();
}

export default function* (url, { anonymous = false, ...options }) {
  const authorization = anonymous ? {} : {
    Authorization: yield effects.select(getToken),
  };

  return yield fetch(formatUrl(url), {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      ...authorization,
    },
    ...options,
  }).then(checkStatus).then(parseJSON);
}
