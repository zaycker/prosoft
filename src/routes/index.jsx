import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, routerRedux } from 'dva/router';
import dynamic from 'dva/dynamic';
import { setPropTypes } from 'recompose';
import {
  HOME,
  LOGIN,
  PROFILE,
  REGISTER,
} from './constants';

const { ConnectedRouter } = routerRedux;

const COMMON_MODELS = [
  import('models/auth'),
];

export default setPropTypes({
  history: PropTypes.shape({}),
  app: PropTypes.shape({}),
})(({ history, app }) => (
  <ConnectedRouter history={history}>
    <Switch>
      <Route
        exact
        path={HOME}
        component={dynamic({
          app,
          models: () => [...COMMON_MODELS],
          component: () => import('containers/Home'),
        })}
      />
      <Route
        exact
        path={LOGIN}
        component={dynamic({
          app,
          models: () => [...COMMON_MODELS],
          component: () => import('containers/Login'),
        })}
      />
      <Route
        exact
        path={PROFILE}
        component={dynamic({
          app,
          models: () => [
            ...COMMON_MODELS,
            import('models/subagents'),
          ],
          component: () => import('containers/Profile'),
        })}
      />
      <Route
        exact
        path={REGISTER}
        component={dynamic({
          app,
          models: () => [...COMMON_MODELS],
          component: () => import('containers/Register'),
        })}
      />
    </Switch>
  </ConnectedRouter>
));
