export const HOME = '/';
export const LOGIN = '/login';
export const PROFILE = '/profile';
export const REGISTER = '/register';

export default {
  HOME,
  LOGIN,
  PROFILE,
  REGISTER,
};
