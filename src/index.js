import 'bootstrap/dist/css/bootstrap.css?raw'; // eslint-disable-line import/no-unresolved
import dva from 'dva';
import createLoading from 'dva-loading';
import createHistory from 'history/createBrowserHistory';

import appModel from 'models/app';
import routes from './routes';

import registerServiceWorker from './registerServiceWorker';

import './styles.less';

// 1. Initialize
const app = dva({
  ...createLoading({
    effects: true,
  }),
  history: createHistory(),
});

// 2. Model
app.model(appModel);

// 3. Router
app.router(routes);

// 4. Start
app.start('#root');

registerServiceWorker();
