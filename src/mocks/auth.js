import fetchMock from 'fetch-mock';

fetchMock.post(/(\/login|\/register)(?:\W|$)/, (_, { body: { email, name = 'Test user' } }) => {
  return {
    success: true,
    token: '123',
    user: {
      email,
      name,
      balance: 1000 + Math.random() * 1000,
    },
  };
});

fetchMock.post(/\/logout(?:\W|$)/, {
  success: true,
});
