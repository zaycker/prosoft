import fetchMock from 'fetch-mock';

fetchMock.get(/\/subagents(?:\W|$)/, {
  success: true,
  data: [{
    id: 1,
    name: 'First agent',
  }, {
    id: 2,
    name: 'Second agent',
  }, {
    id: 3,
    name: 'Third agent',
  }],
});
