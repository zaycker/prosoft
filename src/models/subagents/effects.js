import api from 'services/api';
import {
  API_SUBAGENTS,
} from './constants';
import {
  FETCH_LIST,
  FETCH_LIST_FAILED,
  FETCH_LIST_SUCCESS,
  ADD_SUBAGENT,
  REMOVE_SUBAGENT,
  SEND_MONEY_TO_SUBAGENT,
} from './actions';

export default {
  * updateToken({
    payload,
  }, { put }) {
    yield put({
      type: 'updateState',
      payload,
    });
  },

  * [FETCH_LIST](_, { call, put }) {
    try {
      const data = yield call(api, API_SUBAGENTS, {
        method: 'GET',
      });

      if (data.success) {
        yield put({ type: FETCH_LIST_SUCCESS, payload: data });
      } else {
        throw data;
      }
    } catch (e) {
      yield put({ type: FETCH_LIST_FAILED, payload: e });
    }
  },

  * [ADD_SUBAGENT]({ payload }, { call }) {
    yield call(window.alert, 'Не реализовано');
    try {
      const data = yield call(api, API_SUBAGENTS, {
        method: 'POST',
        body: JSON.stringify(payload),
      });

      if (data.success) {
        // TODO: later
      } else {
        throw data;
      }
    } catch (e) {
      // TODO: later
    }
  },

  * [REMOVE_SUBAGENT](_, { call }) {
    yield call(window.alert, 'Не реализовано');
  },

  * [SEND_MONEY_TO_SUBAGENT](_, { call }) {
    yield call(window.alert, 'Не реализовано');
  },
};
