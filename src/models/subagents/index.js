import modelExtend from 'dva-model-extend';
import commonModel from 'models/common';
import effects from './effects';
import reducers, { initialState } from './reducers';
import {
  MODEL,
} from './constants';

export default modelExtend(commonModel, {
  namespace: MODEL,
  state: initialState,
  effects,
  reducers,
});
