import { createAction } from 'redux-actions';
import { MODEL } from './constants';

export const FETCH_LIST = 'FETCH_LIST';
export const FETCH_LIST_SUCCESS = 'FETCH_LIST_SUCCESS';
export const FETCH_LIST_FAILED = 'FETCH_LIST_FAILED';

export const ADD_SUBAGENT = 'ADD_SUBAGENT';
export const REMOVE_SUBAGENT = 'REMOVE_SUBAGENT';
export const SEND_MONEY_TO_SUBAGENT = 'SEND_MONEY_TO_SUBAGENT';

export const fetchList = createAction(`${MODEL}/${FETCH_LIST}`);
export const addSubagent = createAction(`${MODEL}/${ADD_SUBAGENT}`);
export const removeSubagent = createAction(`${MODEL}/${REMOVE_SUBAGENT}`);
export const sendMoneyToSubagent = createAction(`${MODEL}/${SEND_MONEY_TO_SUBAGENT}`);

export default {};
