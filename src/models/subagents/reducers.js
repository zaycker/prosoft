import {
  FETCH_LIST_FAILED,
  FETCH_LIST_SUCCESS,
} from './actions';

export const initialState = {
  error: null,
  success: false,
  data: [],
};

export default {
  [FETCH_LIST_SUCCESS](state, { payload }) {
    return {
      ...state,
      ...payload,
    };
  },
  [FETCH_LIST_FAILED](state, { payload }) {
    return {
      ...state,
      ...payload,
      success: false,
      data: [],
    };
  },
};
