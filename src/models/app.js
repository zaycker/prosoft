import queryString from 'query-string';
import modelExtend from 'dva-model-extend';
import commonModel from './common';


export default modelExtend(commonModel, {
  namespace: 'app',
  state: {
    locationQuery: {},
  },
  subscriptions: {
    setupHistory({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            locationQuery: queryString.parse(location.search),
          },
        });
      });
    },
  },
});
