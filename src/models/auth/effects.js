import pick from 'lodash/pick';
import { routerRedux } from 'dva/router';
import {
  reset as localStorageReset,
  set as localStorageSet,
} from 'services/localStorage';
import {
  LOGIN as ROUTE_LOGIN,
  PROFILE as ROUTE_PROFILE,
} from 'routes/constants';
import api from 'services/api';
import {
  LOCALSTORAGE_AUTH_KEY,
  API_LOGIN,
  API_LOGOUT,
  API_REGISTER,
} from './constants';
import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGOUT,
  REGISTER,
  REGISTER_FAILED,
  REGISTER_SUCCESS,
} from './actions';

export default {
  * updateToken({
    payload,
  }, { call, put }) {
    if (payload.token) {
      yield call(localStorageSet, LOCALSTORAGE_AUTH_KEY, pick(payload, ['token', 'user']));
    } else {
      yield call(localStorageReset, LOCALSTORAGE_AUTH_KEY);
    }

    yield put({
      type: 'updateState',
      payload: {
        token: '',
        user: {},
        ...pick(payload, ['token', 'user']),
      },
    });
  },

  * [LOGOUT](_, { fork, put }) {
    yield fork(api, API_LOGOUT, { method: 'POST' });
    yield put({ type: 'updateToken', payload: {} });
  },

  * [LOGIN]({ payload }, { call, put }) {
    try {
      const data = yield call(api, API_LOGIN, {
        anonymous: true,
        method: 'POST',
        body: JSON.stringify(payload),
      });

      if (data.success) {
        yield put({ type: LOGIN_SUCCESS, payload: data });
      } else {
        throw data;
      }
    } catch (e) {
      yield put({ type: LOGIN_FAILED, payload: e });
    }
  },

  * [LOGIN_FAILED]({ payload: e }, { put }) {
    yield put({ type: 'updateToken', payload: {} });
    yield put({
      type: 'updateState',
      payload: {
        token: '',
        user: {},
        success: false,
        error: e.message,
      },
    });
  },

  * [LOGIN_SUCCESS]({ payload }, { put, select }) {
    const { locationQuery } = yield select(_ => _.app);
    yield put({ type: 'updateToken', payload });
    yield put({
      type: 'updateState',
      payload: {
        ...payload,
        error: null,
      },
    });
    const { from } = locationQuery;
    if (from && from !== ROUTE_LOGIN) {
      yield put(routerRedux.push(from));
    } else {
      yield put(routerRedux.push(ROUTE_PROFILE));
    }
  },

  * [REGISTER]({ payload }, { call, put }) {
    try {
      const data = yield call(api, API_REGISTER, {
        anonymous: true,
        method: 'POST',
        body: JSON.stringify(payload),
      });

      if (data.success) {
        yield put({ type: REGISTER_SUCCESS, payload: data });
      } else {
        throw data;
      }
    } catch (e) {
      yield put({ type: REGISTER_FAILED, payload: e });
    }
  },
};
