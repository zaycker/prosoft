import get from 'lodash/get';
import identity from 'lodash/identity';
import { createSelector } from 'reselect';
import modelExtend from 'dva-model-extend';
import commonModel from 'models/common';
import { get as localStorageGet } from 'services/localStorage';
import effects from './effects';
import {
  LOCALSTORAGE_AUTH_KEY,
  MODEL,
} from './constants';

export const getToken = createSelector(state => get(state, 'auth.token', ''), identity);

const {
  user: initialUser,
  token: initialToken,
} = localStorageGet(LOCALSTORAGE_AUTH_KEY) || {};

export default modelExtend(commonModel, {
  namespace: MODEL,
  state: {
    error: null,
    success: false,
    token: initialToken,
    user: initialUser,
  },
  effects,
});
