export const MODEL = 'auth';

export const LOCALSTORAGE_AUTH_KEY = 'prosoft-auth';

export const API_LOGOUT = '/logout';
export const API_LOGIN = '/login';
export const API_REGISTER = '/register';

export default {};
