import { connect } from 'dva';
import { Redirect } from 'dva/router';
import {
  LOGIN as ROUTE_LOGIN,
  PROFILE as ROUTE_PROFILE,
} from 'routes/constants';

export default connect(({ auth }) => ({
  to: auth.token ? ROUTE_PROFILE : ROUTE_LOGIN,
}))(Redirect);
