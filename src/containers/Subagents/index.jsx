import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {
  Table,
  Button,
} from 'reactstrap';
import {
  removeSubagent as removeSubagentAction,
  sendMoneyToSubagent as sendMoneyToSubagentAction,
} from 'models/subagents/actions';

const Subagents = ({
  subAgents,
  removeSubagent,
  addSubagent,
  sendMoneyTo,
}) => (
  <Fragment>
    <h2>
      Субагенты
    </h2>
    <Button onClick={addSubagent}>
      Добавить
    </Button>
    <Table>
      <thead>
        <tr>
          <th>
          #
          </th>
          <th>
          Имя
          </th>
          <th />
          <th />
        </tr>
      </thead>
      <tbody>
        {subAgents.map(({ id, name }) => (
          <tr key={id}>
            <td>
              {id}
            </td>
            <td>
              {name}
            </td>
            <td>
              <Button onClick={() => removeSubagent(id)}>
              X
              </Button>
            </td>
            <td>
              <Button onClick={() => sendMoneyTo(id)}>
                →
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  </Fragment>
);

Subagents.propTypes = {
  addSubagent: PropTypes.func,
  subAgents: PropTypes.arrayOf(PropTypes.shape({})),
  removeSubagent: PropTypes.func,
  sendMoneyTo: PropTypes.func,
};

const noop = () => window.alert('Не реализовано');

Subagents.defaultProps = {
  addSubagent: noop,
  subAgents: [],
  removeSubagent: noop,
  sendMoneyTo: noop,
};

export default connect(null, {
  removeSubagent: removeSubagentAction,
  sendMoneyTo: sendMoneyToSubagentAction,
})(Subagents);
