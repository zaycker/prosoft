import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {
  compose,
  setPropTypes,
  withProps,
} from 'recompose';
import {
  Button,
  Container,
  Col,
  Form as BootstrapForm,
  FormGroup,
  FormFeedback,
  Label,
  Row,
} from 'reactstrap';
import { Form, Field } from 'react-final-form';
import Layout from 'containers/Layout';
import { Link } from 'dva/router';
import { REGISTER } from 'routes/constants';
import reduxSubmitHandler from 'enhancers/reduxSubmitHandler';
import InputField from 'components/InputField';
import { login } from 'models/auth/actions';
import {
  required,
} from 'utils/validators';
import styles from './styles.less';

const Login = setPropTypes({
  dirtySinceLastSubmit: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  submitError: PropTypes.string,
})(({
  dirtySinceLastSubmit,
  handleSubmit,
  pristine,
  invalid,
  submitting,
  submitError,
}) => (
  <Layout>
    <Container>
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <BootstrapForm
            className={styles.form}
            onSubmit={handleSubmit}
          >
            <h4>
              Войти
            </h4>
            <FormGroup>
              <Label>
                Email
                <Field
                  autoComplete="email"
                  component={InputField}
                  name="email"
                  placeholder="example@email.com"
                  type="email"
                  validate={required}
                />
              </Label>
            </FormGroup>
            <FormGroup>
              <Label>
                Пароль
                <Field
                  autoComplete="current-password"
                  component={InputField}
                  name="password"
                  placeholder="Password"
                  type="password"
                  validate={required}
                />
              </Label>
            </FormGroup>
            {submitError && !dirtySinceLastSubmit && !submitting && (
            <FormFeedback>
              {submitError}
            </FormFeedback>
            )}
            <Button
              disabled={pristine || (invalid && !dirtySinceLastSubmit) || submitting}
            >
              Войти
            </Button>
            <Link to={REGISTER} className={styles.registerLink}>
              Регистрация
            </Link>
          </BootstrapForm>
        </Col>
      </Row>
    </Container>
  </Layout>
));

export default compose(
  connect(({ auth, loading }) => ({
    isSubmitComplete: !loading.models.auth,
    submitError: auth.error || null,
  }), {
    submitAction: login,
  }),
  withProps({
    component: Login,
  }),
  reduxSubmitHandler,
)(Form);
