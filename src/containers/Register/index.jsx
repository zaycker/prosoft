import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {
  compose,
  setPropTypes,
  withProps,
} from 'recompose';
import {
  Button,
  Form as BootstrapForm,
  FormGroup,
  FormFeedback,
  Label,
  Container,
  Row,
  Col,
} from 'reactstrap';
import { Form, Field } from 'react-final-form';
import InputField from 'components/InputField';
import Layout from 'containers/Layout';
import reduxSubmitHandler from 'enhancers/reduxSubmitHandler';
import { register } from 'models/auth/actions';
import {
  required,
} from 'utils/validators';
import styles from './styles.less';

const Register = setPropTypes({
  dirtySinceLastSubmit: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  submitError: PropTypes.string,
})(({
  dirtySinceLastSubmit,
  handleSubmit,
  pristine,
  invalid,
  submitting,
  submitError,
}) => (
  <Layout>
    <Container>
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <BootstrapForm
            className={styles.form}
            onSubmit={handleSubmit}
          >
            <h4>
              Регистрация
            </h4>
            <FormGroup>
              <Label>
                Имя
                <Field
                  component={InputField}
                  type="text"
                  name="name"
                  placeholder="John"
                  autoComplete="name"
                  validate={required}
                />
              </Label>
            </FormGroup>
            <FormGroup>
              <Label>
                Email
                <Field
                  component={InputField}
                  type="email"
                  name="email"
                  placeholder="example@email.com"
                  autoComplete="email"
                  validate={required}
                />
              </Label>
            </FormGroup>
            <FormGroup>
              <Label>
                Пароль
                <Field
                  name="password"
                  component={InputField}
                  placeholder="Password"
                  type="password"
                  autoComplete="current-password"
                  validate={required}
                />
              </Label>
            </FormGroup>
            {submitError && !dirtySinceLastSubmit && !submitting && (
            <FormFeedback>
              {submitError}
            </FormFeedback>
            )}
            <Button
              disabled={pristine || (invalid && !dirtySinceLastSubmit) || submitting}
            >
              Зарегистрироваться
            </Button>
          </BootstrapForm>
        </Col>
      </Row>
    </Container>
  </Layout>
));

export default compose(
  connect(({ auth, loading }) => ({
    isSubmitComplete: !loading.models.auth,
    submitError: auth.error || null,
  }), {
    submitAction: register,
  }),
  withProps({
    component: Register,
  }),
  reduxSubmitHandler,
)(Form);
