import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Redirect } from 'dva/router';
import omit from 'lodash/omit';
import Layout from 'containers/Layout';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';
import Subagents from 'containers/Subagents';
import { fetchList } from 'models/subagents/actions';
import {
  compose,
  lifecycle,
  mapProps,
  setPropTypes,
} from 'recompose';
import { LOGIN } from 'routes/constants';

const Profile = ({ subAgents, isLogged }) => (
  isLogged ? (
    <Layout>
      <Container>
        <Row>
          <Col md={{ size: 10, offset: 1 }}>
            <Subagents
              subAgents={subAgents}
              removeSubagent={() => {}}
              addSubagent={() => {}}
              sendMoneyTo={() => {}}
            />
          </Col>
        </Row>
      </Container>
    </Layout>
  ) : <Redirect to={LOGIN} />
);

Profile.propTypes = {
  isLogged: PropTypes.bool.isRequired,
  subAgents: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default compose(
  connect(({ subagents, auth }) => ({
    subAgents: subagents.data,
    isLogged: !!auth.token,
  }), {
    fetchSubagents: fetchList,
  }),
  lifecycle({
    componentDidMount() {
      this.props.fetchSubagents();
    },
  }),
  setPropTypes({
    fetchSubagents: PropTypes.func.isRequired,
  }),
  mapProps(props => omit(props, ['fetchSubagents'])),
)(Profile);
