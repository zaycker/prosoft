import React, { Fragment } from 'react';
import { connect } from 'dva';
import PropTypes from 'prop-types';
import {
  Button,
  Nav,
  Navbar,
  NavbarBrand,
  NavItem,
} from 'reactstrap';
import LinkInNavbarBrand from 'components/LinkInNavbarBrand/index';
import { logout as logoutAction } from 'models/auth/actions';
import styles from './styles.less';

const Layout = ({
  children,
  isLogged,
  logout,
  user,
}) => (
  <Fragment>
    <Navbar color="light" light expand="md">
      <NavbarBrand href="/" tag={LinkInNavbarBrand}>
        Просто плати
      </NavbarBrand>
      <Nav className="ml-auto" navbar>
        {user.name && (
          <NavItem className={styles.header}>
            {`Привет, ${user.name}! Баланс ${user.balance && user.balance.toFixed(2)}`}
          </NavItem>
        )}
        {isLogged && (
          <NavItem>
            <Button onClick={logout}>
              Выйти
            </Button>
          </NavItem>
        )}
      </Nav>
    </Navbar>
    {children}
  </Fragment>
);

Layout.propTypes = {
  children: PropTypes.node,
  isLogged: PropTypes.bool,
  logout: PropTypes.func,
  user: PropTypes.shape({}),
};

Layout.defaultProps = {
  children: null,
  isLogged: false,
  logout: () => {},
  user: {},
};

export default connect(({ auth: { token, user } }) => ({
  isLogged: !!token,
  user,
}), {
  logout: logoutAction,
})(Layout);
