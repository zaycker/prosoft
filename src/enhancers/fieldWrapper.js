import { withProps } from 'recompose';

export default withProps(({ input, ...props }) => ({
  ...input,
  ...props,
}));
