import {
  compose,
  lifecycle,
  mapProps,
  withHandlers,
  withState,
} from 'recompose';
import { FORM_ERROR } from 'final-form';
import omit from 'lodash/omit';

/**
 * Enhances react-final-form for working with redux async processes
 * @param submitAction {Function} Submit handler, mandatory.
 */
export default compose(
  withState('submitHandler', 'setSubmitHandler', null),
  withHandlers(({
    onSubmit: ({ submitAction, setSubmitHandler }) => values => new Promise((resolve) => {
      submitAction(values);
      setSubmitHandler({ resolve });
    }),
  })),
  lifecycle({
    componentWillReceiveProps({
      submitHandler,
      setSubmitHandler,
      isSubmitComplete,
      submitError,
    }) {
      if (!isSubmitComplete || !submitHandler) {
        return;
      }

      if (submitError) {
        submitHandler.resolve({ [FORM_ERROR]: submitError });
      } else {
        submitHandler.resolve();
      }
      setSubmitHandler(null);
    },
  }),
  mapProps(props => omit(props, [
    'isSubmitComplete',
    'setSubmitHandler',
    'submitAction',
    'submitError',
    'submitHandler',
  ])),
);
