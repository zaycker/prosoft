## Просто плати

Фронт на `react` + `dvajs` + `reactstrap`.

За основу взят `create-react-app`, выполнен `eject`, обновлены все зависимости до последних версий (в частности, `webpack@4`), переписаны конфиги webpack-а, часть CRA-библиотек (см. `/config/utils`)

`dvajs` взят как абстракция над `redux` (включает `react-router`, `redux-saga`, `fetch`).

`reactstrap` как реализация Bootstrap для react (без jQuery).

для обслуживания форм взят `react-final-form` (от создателя `redux-form`).

доп. слоя абстракции над `reactstrap`-компонентами делать не стал, потому что MVP.

также более-менее активно используется `recompose`, `reselect` и другие. 

## npm сценарии

### `npm start`

Проект запускается в режиме разработки<br>
После запуска перейти на [http://localhost:3000](http://localhost:3000).

### `npm run build`

Сборка в prod-режиме в папку `build`.

