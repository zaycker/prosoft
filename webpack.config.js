const developmentConfig = require('./config/webpack.config.dev');
const productionConfig = require('./config/webpack.config.prod');

module.exports = process.env.NODE_ENV === 'production' ? productionConfig : developmentConfig;
