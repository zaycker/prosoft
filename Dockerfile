FROM nginx

ENV API_ROOT=http://api.test.net/

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY build/ /etc/nginx/html
